#Muchammad Muchib Zainul Fikry
#MCOJT Cyber Security Batch 15
import pymysql
print("Python - PyMYSQL\n")
CreateDB = pymysql.connect(
	host="localhost",
	user="root",
	passwd=""
)

print("Membuat Database dengan nama db_python...")
crtCursor=createDB.cursor()
crtCursor.execute("CREATE database db_python")
createDB.commit()
print("Membuat Database Selesai\n")

mydb = pymysql.connect(
	host="localhost",
	user="root",
	passwd="",
	database="db_python"
)
myCursor = mydb.cursor()

print("Membuat tabel dengan nama tb_datapy dan field id, nilai, dan nilai2")
myCursor.execute("CREATE table tb_datapy (id INT AUTO_INCREMENT, niali1 VARCHAR(255), nilai2 INT(11) NOT NULL, PRIMARYKEY (id))")
print("Membuat Tabel dan Field Selesai\n")

print("Memasukkan Record...")
myCursor.execute("INSERT INTO tb_datapy(nilai1, nilai2) VALUES ('Ini data PyMySQL 1', 111)")
myCursor.execute("INSERT INTO tb_datapy(nilai1, nilai2) VALUES ('Ini data PyMySQL 2', 222)")
myCursor.execute("INSERT INTO tb_datapy(nilai1, nilai2) VALUES ('Ini data PyMySQL 2', 333)")

print("Menampilkan Seluruh Record....")
myCursor.execute("SELECT * FROM tb_datapy")
record = myCursor.fetchall()
print("Record:")
for x in record :
	print(x)
mydb.commit()